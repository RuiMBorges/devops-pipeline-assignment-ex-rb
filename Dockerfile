# Create image from maven image (https://hub.docker.com/_/maven/)
FROM maven:3-jdk-8

RUN java -version
RUN mvn --version

RUN apt-get update

COPY . /devops-pipeline-assignment-ex-rb

WORKDIR /devops-pipeline-assignment-ex-rb

RUN mvn install