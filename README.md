## Example Java Application

Simple java application with different types of tests.

### You can run unit tests with:
```
mvn test
```

### You can run integration tests with:
```
mvn verify -P integration-test
```    

### Create a jar with all dependencies with:
```
mvn package
```   
(this will create the `target/maven-integration-testing-jar-with-dependencies.jar` file)
#Assignment:

## Part 1

Create a Docker Image to run the application:

1. Create a Dockerfile such that the image downloads and caches all maven dependencies (to do this, it should run `mvn install`). This image will be based on the `maven:3-jdk-8` image (https://hub.docker.com/_/maven/)
2. Build the image and push it to Docker Hub.

## Part 2
Create a Jenkins Pipeline with, at least, the following pipeline steps:

* **Unit Tests**: Run unit tests (use the docker image created in Part 1);
* **Integration Tests**: Run integration tests (use the docker image created in Part 1);
* **Package**: Create a jar with all dependencies (use the docker image created in Part 1);
* **Docker Push**: After [a confirmation from the user](https://jenkins.io/doc/pipeline/steps/pipeline-input-step/), push, to Docker Hub, an image that runs the java application in the jar file (resulting from the previous step);

Also, create a MYREADME file with the following information:

* Your identification
* The docker command to run the container pushed to Docker Hub in step **Docker Push**. Assume we want to map port 22 of the container to port 2222 of the docker host.

NOTE: If you could not complete Part I, a docker image to run the tests was created. It is available at: `npereira/jenkins-assignment`. **Use it ONLY if you could not complete Part 1** 

### What to deliver
In [moodle](https://moodle.isep.ipp.pt/course/view.php?id=6345), deliver the following files:

* Jenkinsfile(s) and Dockerfile(s) you might have created.
* MYREADME file
